require asyn,4.36.0
require ADCore,3.7.0
require NDDriverStdArrays,9cbc817
require ADSupport,1.9.0
require calc,3.7.3
require sequencer,2.2.7
require busy,1.7.2-e45eda2
require waveanalyzer,develop


# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximim image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "8000")
# The maximim image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "1")

# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "8000")

# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "500")

# The number of elements in the driver waveform record
epicsEnvSet("NELEMENTS", "8000")
# The datatype of the waveform record
epicsEnvSet("FTVL", "DOUBLE")
# The asyn interface waveform record
epicsEnvSet("TYPE", "Float64")

#asynSetMinTimerPeriod(0.001)

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")
epicsEnvSet("EPICS_CA_ADDR_LIST", "10.0.16.92 localhost")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")

###########################################################

# Prefix for all records
epicsEnvSet("P1", "FIM_SCOPE1:")
epicsEnvSet("R1", "CH1:")
# The port name for the detector
epicsEnvSet("PORT1",   "NDSA1")
# The input waveform record
epicsEnvSet("INP_WAVE_1", "FIM:AI:CH10")

epicsEnvSet("PREFIX", "$(P1)")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh", "P=$(P1), R=$(R1), PORT=$(PORT1), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), FTVL=$(FTVL), TYPE=$(TYPE), UNIT=CMPPORT1, STAPORT=Wave1")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh", "P=$(P1), R=$(R1), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_1)")

###########################################################

# Prefix for all records
epicsEnvSet("P2", "FIM_SCOPE2:")
epicsEnvSet("R2", "CH2:")
# The port name for the detector
epicsEnvSet("PORT2",   "NDSA2")
# The input waveform record
epicsEnvSet("INP_WAVE_2", "FIM:AI:CH11")

epicsEnvSet("PREFIX", "$(P2)")
iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh", "P=$(P2), R=$(R2), PORT=$(PORT2), QSIZE=$(QSIZE), NELEMENTS=$(NELEMENTS), FTVL=$(FTVL), TYPE=$(TYPE), UNIT=CMPPORT2, STAPORT=Wave2")
iocshLoad("$(waveanalyzer_DIR)connectPV.iocsh", "P=$(P2), R=$(R2), NELEMENTS=$(NELEMENTS), INP_WAVE=$(INP_WAVE_2)")

###########################################################


iocInit()



