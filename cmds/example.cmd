require asyn,4.37.0
require adcore,3.9.0
require admisc,2.1.1
require nddriverstdarrays,9cbc817
require adsupport,1.9.0
require calc,3.7.3
require sequencer,2.2.7
require busy,1.7.2-e45eda2
require waveanalyzer,develop

# Prefix for all records
epicsEnvSet("PREFIX", "LLRF:")
# The port name for the detector
epicsEnvSet("PORT",   "NDSA")

# The queue size for all plugins
epicsEnvSet("QSIZE",  "20")
# The maximim image width; used to set the maximum size for this driver and for row profiles in the NDPluginStats plugin
epicsEnvSet("XSIZE",  "35000")
# The maximim image height; used to set the maximum size for this driver and for column profiles in the NDPluginStats plugin
epicsEnvSet("YSIZE",  "1")

# The maximum number of time series points in the NDPluginStats plugin
epicsEnvSet("NCHANS", "8000")

# The maximum number of frames buffered in the NDPluginCircularBuff plugin
epicsEnvSet("CBUFFS", "35000")

# The number of elements in the driver waveform record
epicsEnvSet("NELEMENTS", "35000")
# The datatype of the waveform record
epicsEnvSet("FTVL", "DOUBLE")
# The asyn interface waveform record
epicsEnvSet("TYPE", "Float64")

#asynSetMinTimerPeriod(0.001)

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "100000000")
epicsEnvSet("EPICS_CA_ADDR_LIST", "172.30.9.11  localhost")
epicsEnvSet("EPICS_CA_AUTO_ADDR_LIST", "NO")

epicsEnvSet("INP_WAVE", "OCTOPUS-1:DWN0-CMP0")
epicsEnvSet("OUT_WAVE", "$(PREFIX)LAB:ArrayIn")

# Create an NDDriverStdArrays drivers
# NDDriverStdArraysConfig(portName, maxBuffers, maxMemory, priority, stackSize)
NDDriverStdArraysConfig("$(PORT)", $(QSIZE), 0, 0)
dbLoadRecords("NDDriverStdArrays.db","P=$(PREFIX),R=LAB:,PORT=$(PORT),ADDR=0,TIMEOUT=1,NELEMENTS=$(NELEMENTS),TYPE=$(TYPE),FTVL=$(FTVL)")

# Create a standard arrays plugin, set it to get data from the NDDriverStdArrays driver.
NDStdArraysConfigure("Wave1", 3, 0, "$(PORT)", 0)
dbLoadRecords("NDStdArrays.template", "P=$(PREFIX),R=image1:,PORT=Wave1,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=35000")

# load common plugins
iocshLoad("$(adcore_DIR)commPlugins.iocsh", "UNIT=TESTJP")

dbLoadRecords("waveanalyzer.template", "P=$(PREFIX), R=LAB:, MAX_NELM=$(NELEMENTS), INP_WAVE=$(INP_WAVE), OUT_WAVE=$(OUT_WAVE)")
#iocshLoad("$(waveanalyzer_DIR)createDriver.iocsh", "P=$(PREFIX), R=LAB:, MAX_NELM=$(NELEMENTS)")

iocInit()

dbpf $(PREFIX)LAB:DataType Float64
dbpf $(PREFIX)LAB:NDimensions 2
system "caput -a $(PREFIX)LAB:Dimensions 2 35000 1"


